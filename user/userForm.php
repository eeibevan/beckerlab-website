<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>User</title>

    <?php require_once $config['serverRoot'] .'/partials/scriptAndCss.php' ?>

    <script>
        function submitUpdate(data, done) {
            $.ajax({
                type:'POST',
                url:"<?=$config['webRoot']?>index.php?path=/api/user/update",
                dataType:'json',
                statusCode: {
                    401: status401Handler
                },
                data: data
            }).done(function (data, textStatus, jqXHR) {
                done(data, textStatus, jqXHR);
            }).fail(function (jqXHR, textStatus, errorThrown) {
                baseAjaxErrorHandler(jqXHR)
            })
        }

        function submitAdd(data, done) {
            $.ajax({
                type:'POST',
                url:"<?=$config['webRoot']?>index.php?path=/api/user/add",
                dataType:'json',
                statusCode: {
                    401: status401Handler
                },
                data: data
            }).done(function (data, textStatus, jqXHR) {
                done(data, textStatus, jqXHR);
            }).fail(function (jqXHR, textStatus, errorThrown) {
                baseAjaxErrorHandler(jqXHR)
            })
        }

        $(document).ready(function() {
            var form = $('#userForm');

            $("#host").change(function () {
                var password = $("#password");
                if ($("#host").val() === "clarion.edu") {
                    password.prop('disabled', true);
                    password.prop('required', false);
                } else {
                    password.prop('disabled', false);
                    password.prop('required', true);
                }
            });

            form.on('submit', function(event) {
                // Prevent Chrome Default Behavior of Submitting A From Whenever Any Button Is Clicked
                event.preventDefault();
                event.stopPropagation();

                form.addClass('was-validated');

                // Stop Invalid Data From Submitting
                if (form[0].checkValidity() === false)
                    return;


                var data = {
                    <?php if (isset($user)) { ?>
                    id: <?php echo $user->id ?>,
                    <?php } ?>
                    username: $("#username").val().trim(),
                    host: $("#host").val(),
                    email: $("#email").val().trim(),
                    role: $("#role").val(),
                    isActive: $('#isActive').is(':checked')
                };

                var password = $("#password").val();
                if (password.length)
                    data.password = password;

                var onSuccess = function (data, textStatus, jqXHR) {
                    window.location.replace("<?=$config['webRoot']?>index.php?path=/user/all");
                };

                <?php if (isset($user)) { ?>
                submitUpdate(data, onSuccess);
                <?php } else { ?>
                submitAdd(data, onSuccess);
                <?php } ?>
            });
        });
    </script>
</head>
<body>

<?php require_once $config['serverRoot'] . '/partials/nav.php' ?>

<main role="main" class="container">
    <div class="offset-lg-3">
        <?php if (isset($user)) {?>
            <h1>Edit User</h1>
        <?php } else { ?>
            <h1>Create User</h1>
        <?php } ?>
        <form class="form" id="userForm" novalidate>

            <div class="form-group row col-lg-6">
                <label for="username">Username</label>
                <input id="username" type="text" autocomplete="off" class="form-control" placeholder="fsradmin" value="<?php if (isset($user)) { echo htmlspecialchars($user->username); }?>" required>
                <div class="invalid-feedback">Username is required</div>
            </div>
            <div class="form-group row col-lg-6">
                <label for="host">Host</label>
                <select class="form-control" id="host" required>
                    <option <?php if (isset($user) && $user->host === 'localhost') echo 'selected';?> value="localhost">localhost</option>
                    <option <?php if (isset($user) && $user->host === 'clarion.edu') echo 'selected';?> value="clarion.edu">clarion.edu</option>
                </select>
                <div class="invalid-feedback">Host is required</div>
            </div>
            <div class="form-group row col-lg-6">
                <label for="password">Password</label>
                <input type="password" autocomplete="off" class="form-control" id="password" placeholder="password"
                    <?php
                    if (!isset($user))
                        echo 'required';
                    if (isset($user) && $user->host === 'clarion.edu')
                        echo 'disabled';
                    ?> />
                <div class="invalid-feedback">Password is required</div>
            </div>
            <div class="form-group row col-lg-6">
                <label for="email">Email</label>
                <input id="email" type="text" autocomplete="off" class="form-control" placeholder="beckerlab@clarion.edu" value="<?php if (isset($user)) { echo htmlspecialchars($user->email); }?>" required>
                <div class="invalid-feedback">Email is required</div>
            </div>
            <div class="form-group row col-lg-6">
                <label for="role">Role</label>
                <select class="form-control" id="role" required>
                    <option value="fsr" <?php if (isset($user) && $user->role === 'fsr') echo 'selected';?> >FSR</option>
                    <option value="faculty" <?php if (isset($user) && $user->role === 'faculty') echo 'selected';?>>Faculty</option>
                </select>
                <div class="invalid-feedback">Role is required</div>
            </div>
            <div class="form-group row col-lg-6 justify-content-center">
                <div>
                    <input class="form-check-input big-checkbox" type="checkbox" value="isActive" id="isActive" <?php if (isset($user->isActive) && $user->isActive || !isset($user)) echo 'checked'; ?>>
                </div>
                <div>
                    <label style="font-size: 20px;" class="form-check-label" for="isActive">
                        Active
                    </label>
                </div>
            </div>


            <?php if (isset($user)) {?>
                <button class="btn btn-lg btn-primary offset-lg-4" type="submit">Edit User</button>
            <?php } else { ?>
                <button class="btn btn-lg btn-primary offset-lg-4" type="submit">Create User</button>
            <?php } ?>

        </form>
    </div>

</main>
<?php require_once $config['serverRoot'] . '/partials/footer.php' ?>
</body>
</html>