<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <?php require_once $config['serverRoot'] .'/partials/scriptAndCss.php' ?>

    <title>Error</title>
</head>
<body>

<?php require_once $config['serverRoot'] . '/partials/nav.php' ?>

<main role="main" class="container_replacement">
    <div class="titleCentering">
        <h1>Error</h1>

        <h5>
            <?php echo (isset($errorMessage)) ? $errorMessage : "Unknown Error" ?>
        </h5>
    </div>
</main>

<?php require_once $config['serverRoot'] . '/partials/footer.php' ?>
</body>
</html>
