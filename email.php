<?php

namespace email
{

    class EmailException extends \Exception {
        public function __construct($message, $code = 0, \Exception $previous = null) {
            parent::__construct($message, $code, $previous);
        }

        public function __toString() {
            return __CLASS__ . ": [{$this->code}]: {$this->message}\n";
        }
    }

    function _makeFactory(array $config) {
        $mailConfig["host"] = $config['emailHost'];
        $mailConfig["port"] = $config['emailPort'];
        $mailConfig["auth"] = true;
        $mailConfig["username"] = $config['emailUsername'];
        $mailConfig["password"] = $config['emailPassword'];

        return \Mail::factory('smtp', $mailConfig);
    }

    /**
     * @param string $email
     * Faculty To Send The Email To
     *
     * @param \Lab $reservation
     * The Reservation We Are Notifying The
     * Faculty Member About
     *
     * @param array $config
     * Configuration From config.php
     * Draws Email Credentials, Additional CC's
     *
     * @throws EmailException
     */
    function singleReservation(string $email, \Lab $reservation, array $config) {
        require_once "Mail.php";

        $lab = ($reservation->is_teaching_lab) ? 'Teaching Lab':'Outside Teaching Lab';

        $from  = "Becker Lab <cubeckerlab@gmail.com>";
        $cc = implode(',', $config['emailAdditionalCc']);
        $recipients = array_merge([$email], $config['emailAdditionalCc']);
        $subject = "Lab Reservation Created";

        $body = "<div>Your reservation for " . $reservation->date->format('m/d') . " was created." . '</div>'.
            "<div>Here's what we got:<div>" .
            "<div style='text-indent:50px;'>Class: " . htmlspecialchars($reservation->dept . ' ' . $reservation->reserving_class . ' ' . $reservation->reserving_section) . '</div>' .
            "<div style='text-indent:50px;'>Lab: " . $lab . '</div>' .
            "<div style='text-indent:50px;'>Start Time: " . $reservation->start_time->format('g:ia') . '</div>' .
            "<div style='text-indent:50px;'>End Time: " .  $reservation->end_time->format('g:ia') . '</div>' .
            '<br />' .
            '<div>If you have any questions/concerns please email us at: beckerlab@clarion.edu' . '</div>';

        $headers = [
            'To' => [$email],
            'From' => $from,
            'Cc' => $cc,
            'Subject' => $subject,
            'MIME-Version' => 1,
            'Content-type' => 'text/html;charset=iso-8859-1'
        ];

        $smtp = _makeFactory($config);


        $mail = $smtp->send($recipients, $headers, $body);

        if(\PEAR::isError($mail)) {
            throw new EmailException($mail->getMessage());
        }
    }
    function updateSingleReservation(string $email, \Lab $reservation, array $config) {
        require_once "Mail.php";

        $lab = ($reservation->is_teaching_lab) ? 'Teaching Lab':'Outside Teaching Lab';

        $from  = "Becker Lab <cubeckerlab@gmail.com>";
        $cc = implode(',', $config['emailAdditionalCc']);
        $recipients = array_merge([$email], $config['emailAdditionalCc']);
        $subject = "Lab Reservation Updated";

        $body = "<div>Your reservation for " . $reservation->date->format('m/d') . " was updated." . '</div>'.
            "<div>Here's what we got:<div>" .
            "<div style='text-indent:50px;'>Class: " . htmlspecialchars($reservation->dept . ' ' . $reservation->reserving_class . ' ' . $reservation->reserving_section) . '</div>' .
            "<div style='text-indent:50px;'>Lab: " . $lab . '</div>' .
            "<div style='text-indent:50px;'>Start Time: " . $reservation->start_time->format('g:ia') . '</div>' .
            "<div style='text-indent:50px;'>End Time: " .  $reservation->end_time->format('g:ia') . '</div>' .
            '<br />' .
            '<div>If you have any questions/concerns please email us at: beckerlab@clarion.edu' . '</div>';

        $headers = [
            'To' => [$email],
            'From' => $from,
            'Cc' => $cc,
            'Subject' => $subject,
            'MIME-Version' => 1,
            'Content-type' => 'text/html;charset=iso-8859-1'
        ];

        $smtp = _makeFactory($config);


        $mail = $smtp->send($recipients, $headers, $body);

        if(\PEAR::isError($mail)) {
            throw new EmailException($mail->getMessage());
        }
    }

    /**
     * @param string $email
     * @param \Repeating_Reservation $repeating_res
     * @param string $lab
     * @param array $labReservations
     * @param array $conflicts
     * @param array $config
     * @throws EmailException
     */
    function repeatingReservation(string $email, \Repeating_Reservation $repeating_res, string $lab, array $labReservations, array $conflicts, array $config) {
        require_once "Mail.php";

        $from  = "Becker Lab <cubeckerlab@gmail.com>";
        $cc = implode(',', $config['emailAdditionalCc']);
        $recipients = array_merge([$email], $config['emailAdditionalCc']);
        $subject = "Lab Reservation Created";

        $body = "<div>Your reservation for " . $repeating_res->start_date->format('m/d') .' to ' . $repeating_res->end_date->format('m/d') . " was created." . '</div>'.
            "<div>Here's what we got:<div>" .
            "<div style='text-indent:50px;'>Class: " . htmlspecialchars($repeating_res->reserving_class) . '</div>' .
            "<div style='text-indent:50px;'>Lab: " . $lab . '</div>' .
            "<div style='text-indent:50px;'>Start Time: " . $repeating_res->start_time->format('g:ia') . '</div>' .
            "<div style='text-indent:50px;'>End Time: " .  $repeating_res->end_time->format('g:ia') . '</div>' .
            "<br />";


        if (count($labReservations) === 0) {
            $body .= "<div>No dates were reserved.</div>";
        } else {
            $body .= "<div>The following dates were reserved: </div>";

            $msgReservedDates = "<ul>";
            foreach ($labReservations as $labReservation) {
                $msgReservedDates .= "<li>" . $labReservation->date->format('m/d') . "</li>";
            }
            $msgReservedDates .= "</ul>";

            $body .= $msgReservedDates . "<br />";
        }

        if (count($conflicts) > 0) {
            $body .= "<div>The following dates had <b>CONFLICTS</b> with other reservations and were not added:</div>";

            $msgConflicts = "<ul>";
            foreach ($conflicts as $conflict) {
                $msgConflicts .= "<li>" . $conflict->format('m/d') . "</li>";
            }
            $msgConflicts .= "</ul>";

            $body .= $msgConflicts;
            $body .= "<br />";
        }

        $body .='<div>If you have any questions/concerns please email us at: beckerlab@clarion.edu' . '</div>';

        $headers = [
            'To' => [$email],
            'From' => $from,
            'Cc' => $cc,
            'Subject' => $subject,
            'MIME-Version' => 1,
            'Content-type' => 'text/html;charset=iso-8859-1'
        ];

        $smtp = _makeFactory($config);

        $mail = $smtp->send($recipients, $headers, $body);

        if(\PEAR::isError($mail)) {
            throw new EmailException($mail->getMessage());
        }
    }

    /**
     * @param string $email
     * Faculty To Send The Email To
     *
     * @param \Request $driveRequest
     * The Request We Are Notifying The
     * Faculty Member About
     *
     * @param array $config
     * Configuration From config.php
     * Draws Email Credentials, Additional CC's
     *
     * @throws EmailException
     */
    function driveRequestCreated(string $email, \Request $driveRequest, array $config) {
        require_once "Mail.php";

        $from  = "Becker Lab <cubeckerlab@gmail.com>";
        $cc = implode(',', $config['emailAdditionalCc']);
        $recipients = array_merge([$email], $config['emailAdditionalCc']);
        $subject = "Drive Request For " . htmlspecialchars($driveRequest->class) . " Was Created";

        $body = "<div>Your reservation for " . htmlspecialchars($driveRequest->class) . " was created." . '</div>'.
            "<div>Here's what we got:<div>" .
            "<div style='text-indent:50px;'>Status: " . htmlspecialchars($driveRequest->status) . '</div>' .
            "<div style='text-indent:50px;'>Number of Drives: " . $driveRequest->drives . '</div>' .
            "<div style='text-indent:50px;'>Operating System: " . htmlspecialchars($driveRequest->operatingSystem) . '</div>' .
            "<div>With the following requirements:<br /><pre>" . htmlspecialchars($driveRequest->other) . '</pre></div>' .
            '<br />' .
            '<div>If you have any questions/concerns please email us at: beckerlab@clarion.edu' . '</div>';

        $headers = [
            'To' => [$email],
            'From' => $from,
            'Cc' => $cc,
            'Subject' => $subject,
            'MIME-Version' => 1,
            'Content-type' => 'text/html;charset=iso-8859-1'
        ];

        $smtp = _makeFactory($config);


        $mail = $smtp->send($recipients, $headers, $body);

        if(\PEAR::isError($mail)) {
            throw new EmailException($mail->getMessage());
        }
    }

    /**
     * @param string $email
     * Faculty To Send The Email To
     *
     * @param \Request $driveRequest
     * The Request We Are Notifying The
     * Faculty Member About
     *
     * @param array $config
     * Configuration From config.php
     * Draws Email Credentials, Additional CC's
     *
     * @throws EmailException
     */
    function driveRequestUpdated(string $email, \Request $driveRequest, array $config) {
        require_once "Mail.php";

        $from  = "Becker Lab <cubeckerlab@gmail.com>";
        $cc = implode(',', $config['emailAdditionalCc']);
        $recipients = array_merge([$email], $config['emailAdditionalCc']);
        $subject = "Drive Request For " . htmlspecialchars($driveRequest->class). " Was Updated";

        $body = "<div>Your reservation for " . htmlspecialchars($driveRequest->class) . " was updated." . '</div>'.
            "<div>Here's what we got:<div>" .
            "<div style='text-indent:50px;'>Status: " . htmlspecialchars($driveRequest->status) . '</div>' .
            "<div style='text-indent:50px;'>Number of Drives: " . $driveRequest->drives . '</div>' .
            "<div style='text-indent:50px;'>Operating System: " . htmlspecialchars($driveRequest->operatingSystem) . '</div>' .
            "<div>With the following requirements:<br /><pre>" . htmlspecialchars($driveRequest->other) . '</pre></div>' .
            '<br />' .
            '<div>If you have any questions/concerns please email us at: beckerlab@clarion.edu' . '</div>';

        $headers = [
            'To' => [$email],
            'From' => $from,
            'Cc' => $cc,
            'Subject' => $subject,
            'MIME-Version' => 1,
            'Content-type' => 'text/html;charset=iso-8859-1'
        ];

        $smtp = _makeFactory($config);


        $mail = $smtp->send($recipients, $headers, $body);

        if(\PEAR::isError($mail)) {
            throw new EmailException($mail->getMessage());
        }
    }
}
