<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>Contact</title>

    <?php require_once $config['serverRoot'] .'/partials/scriptAndCss.php' ?>
    <script src="https://code.jquery.com/jquery-3.2.1.min.js" crossorigin="anonymous"></script>

    <script>
        $(document).ready(function () {
            if (Math.random() > .9) {
                $("#ethanName").html("420, Weeblord")
            }
        })
    </script>

</head>
<body>

<?php require_once $config['serverRoot'] . '/partials/nav.php' ?>

<main role="main" class="container align-content-center align-items-center">
    <h3 style="cursor: pointer" onclick="window.location.href = './index.php?path=/chicken'" >Lab Employees</h3>
    <table class="table table-striped table-sm table-responsive-sm">
        <thead>
        <tr>
            <th>Name</th>
            <th>Email</th>
        </tr>
        </thead>
        <tr>
        	<td>O'Donnell, David</td>
        	<td>D.M.ODonnell@eagle.clarion.edu</td>
        </tr>
        <tr>
        	<td>Hinderliter, Parker</td>
        	<td>P.D.Hinderliter@eagle.clarion.edu</td>
        </tr>
        <tr>
        	<td>Vigus, Trey</td>
        	<td>T.M.Vigus@eagle.clarion.edu</td>
        </tr>
        <tr>
        	<td>Csorba, Thomas</td>
        	<td>T.L.Csorba@eagle.clarion.edu</td>
        </tr>
        <tr>
        	<td>Switzer, Collin</td>
        	<td>C.R.Switzer1@eagle.clarion.edu</td>
        </tr>
        <tr>
        	<td>Combetty, Kalyn</td>
        	<td>K.E.Combetty@eagle.clarion.edu</td>
        </tr>
        <tr>
        	<td>Yount, Mark</td>
        	<td>M.R.Yount@eagle.clarion.edu</td>
        </tr>
    </table>

    <h3 style="margin-top: 75px">FSR Employees</h3>
    <table class="table table-striped table-sm table-responsive-sm">
        <thead>
        <tr>
            <th scope="col">Name</th>
            <th scope="col">Email</th>
        </tr>
        </thead>
        <tr>
            <td>Georgvich, John</td>
            <td>J.L.Georgvich@eagle.clarion.edu</td>
        </tr>
        <tr>
            <td id="ethanName">Dyer, Ethan</td>
            <td>E.J.Dyer@eagle.clarion.edu</td>
        </tr>
        <tr>
        	<td>Maitland, Keith</td>
        	<td>K.H.Maitland@eagle.clarion.edu</td>
        </tr>
    </table>
</main>

<?php require_once $config['serverRoot'] . '/partials/footer.php' ?>
</body>
</html>