<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>Schedule Editor</title>

    <?php require_once $config['serverRoot'] .'/partials/scriptAndCss.php' ?>

    <?php
    /**
     * @param int $dow
     * @return string
     * @throws Exception
     */
    function dayOfWeekToStr(int $dow) {
        switch ($dow) {
            case 1:
                return "monday";
                break;
            case 2:
                return "tuesday";
                break;
            case 3:
                return "wednesday";
                break;
            case 4:
                return "thursday";
                break;
            case 5:
                return "friday";
            default:
                throw new Exception("Failed To Convert Day Of Week");
        }
    }
    ?>

    <script>
        function scheduleChangeHandler() {
            var fsrSchedule = $("#fsrSchedule");
            var labSchedule = $("#schedule");

            if ($("#lab").is(":checked")) {
                labSchedule.show();
                fsrSchedule.hide();
            } else {
                fsrSchedule.show();
                labSchedule.hide();
            }
        }

        $(document).ready(function () {
            $( "#startDate").datepicker({
                minDate: 0,
                maxDate: "+1W"
            });
            $( "#endDate").datepicker({
                minDate: 0
            });
            $("#lab").change(scheduleChangeHandler);
            $("#fsr").change(scheduleChangeHandler);

            var employeeListItems = [];

            // Use input Instead of change Since
            // change In jQuery Only Fires When
            // The Element Looses Focus
            // See: https://gist.github.com/brandonaaskov/1596867
            $("#forNameSearch").bind('input', function (e) {
                var str = $(e.target).val().trim().toLowerCase();
                var splitStr = str.split(' ');

                for (var i = 0; i < employeeListItems.length; i++) {
                    var item = employeeListItems[i];

                    // If We Have An Empty String, Match All
                    if (str.length === 0) {
                        item.element.show();
                        continue;
                    }

                    var containStr = false;
                    for (var j = 0; j < splitStr.length; j++) {
                        var s = splitStr[j];
                        if (item.firstName.toLowerCase().indexOf(s) !== -1 || item.lastName.toLowerCase().indexOf(s) !== -1 ) {
                            containStr = true;
                            break;
                        }
                    }

                    if (containStr) {
                        item.element.show();
                    } else {
                        item.element.hide();
                    }
                }

            });

            // https://github.com/bevacqua/dragula
            var employeeList = $("#employeeList");
            var labScheduleTable = $("#schedule").find("td").toArray();
            var fsrScheduleTable = $("#fsrSchedule").find('td').toArray();

            var containers = [employeeList[0]];
            labScheduleTable.forEach(function (value) {
                containers.push(value)
            });

            fsrScheduleTable.forEach(function (value) {
                containers.push(value)
            });

            var drake;
            $.ajax({
                type:'GET',
                url:'<?=$config['webRoot']?>index.php?path=/api/employee',
                dataType:'json',
                data: {
                    activeOn: moment().format('MM-DD-YYYY')
                },
                statusCode: {
                    401: status401Handler
                }
            }).done(function (data, textStatus, jqXHR) {
                var acc = "";
                for (var i = 0; i < data.length; i++) {
                    acc += '<div ' + 'data-employee-id="' + data[i].employeeId + '" '
                        + 'data-first-name="' + data[i].firstName + '" '
                        + 'data-last-name="' + data[i].lastName + '" '
                        + 'class=\"text-nowrap draggable btn btn-block btn-secondary\"> '
                        + data[i].firstName + ' ' + data[i].lastName
                        + ' </div>'
                }
                employeeList.html(acc);
                employeeListItems = employeeList.find('.draggable').toArray();
                for (var i = 0; i < employeeListItems.length; i++) {
                    var element = $(employeeListItems[i]);
                    employeeListItems[i] = {
                        firstName: element.data('first-name'),
                        lastName: element.data('last-name'),
                        element: element
                    };
                }

                drake = dragula({
                    containers: containers,
                    copy: function (el, source) {
                        // Only Copy From The Original Source
                        // Not The Schedule Itself
                        return source.id === "employeeList"
                    },
                    accepts: function (el, target, source, sibling) {
                        return target.id !== "employeeList";
                    },
                    removeOnSpill: true
                });
            });


            $("#saveBtn").click(function () {
                var saveBtn = $("#saveBtn");
                saveBtn.prop('disabled', true);

                var startStr = $("#startDate").val();
                if (startStr.length === 0) {
                    alert('Start Date Is Required');
                    return;
                }
                var startDate = moment(startStr, "MM-DD-YYYY");
                if (!startDate.isValid()) {
                    alert('Start Date Is Not A Valid Date');
                    return;
                }

                var endStr = $("#endDate").val();
                if (endStr.length === 0) {
                    alert('End Date Is Required');
                    return;
                }
                var endDate = moment(endStr, "MM-DD-YYYY");
                if (!endDate.isValid()) {
                    alert('End Date Is Not A Valid Date');
                    return;
                }

                var scheduleDays = {
                    monday: [],
                    tuesday: [],
                    wednesday: [],
                    thursday: [],
                    friday: []
                };

                function appendItem(item, type) {
                    var jItem = $(item);
                    var employeeDivs = jItem.find('div').toArray();

                    for (var i = 0; i < employeeDivs.length; i++) {
                        var employeeId = $(employeeDivs[i]).data('employee-id');

                        var day = jItem.data('day');
                        var time = parseInt(jItem.data('time'));

                        var record = {
                            employeeId: employeeId,
                            time: time,
                            type: type
                        };

                        switch (day) {
                            case 'monday':
                                scheduleDays.monday.push(record);
                                break;
                            case 'tuesday':
                                scheduleDays.tuesday.push(record);
                                break;
                            case 'wednesday':
                                scheduleDays.wednesday.push(record);
                                break;
                            case 'thursday':
                                scheduleDays.thursday.push(record);
                                break;
                            case 'friday':
                                scheduleDays.friday.push(record);
                                break;
                            default:
                                break;
                        }
                    }
                }

                labScheduleTable.forEach(function (item) {
                    appendItem(item, 'LAB');
                });
                fsrScheduleTable.forEach(function (item) {
                    appendItem(item, 'FSR');
                });


                /**
                 * Sorts on shift start time, ascending
                 */
                function sortShifts(left, right) {
                    return left.time - right.time;
                }

                scheduleDays.monday.sort(sortShifts);
                scheduleDays.tuesday.sort(sortShifts);
                scheduleDays.wednesday.sort(sortShifts);
                scheduleDays.thursday.sort(sortShifts);
                scheduleDays.friday.sort(sortShifts);

                var employeeShiftMap = {};
                function mapEmployeeDays(item, dow) {
                    if (!employeeShiftMap.hasOwnProperty(item.employeeId)) {
                        employeeShiftMap[item.employeeId] = {}
                    }

                    if (!employeeShiftMap[item.employeeId].hasOwnProperty(dow)) {
                        employeeShiftMap[item.employeeId][dow] = [];
                    }

                    employeeShiftMap[item.employeeId][dow].push(item);
                }

                scheduleDays.monday.forEach(function (item) { mapEmployeeDays(item, 1) });
                scheduleDays.tuesday.forEach(function (item) { mapEmployeeDays(item, 2) });
                scheduleDays.wednesday.forEach(function (item) { mapEmployeeDays(item, 3) });
                scheduleDays.thursday.forEach(function (item) { mapEmployeeDays(item, 4) });
                scheduleDays.friday.forEach(function (item) { mapEmployeeDays(item, 5) });


                $.ajax({
                    type:'POST',
                    url:'<?=$config['webRoot']?>index.php?path=/api/schedule/create',
                    data: {
                        start: startStr,
                        end: endStr,
                        schedule: employeeShiftMap
                    },
                    dataType:'json',
                    statusCode: {
                        401: status401Handler
                    }
                }).done(function (data, textStatus, jqXHR) {
                    window.location.href = '<?=$config['webRoot']?>index.php?path=/schedule';
                    saveBtn.prop('disabled', false);
                }).fail(function (jqXHR, textStatus, errorThrown) {
                    baseAjaxErrorHandler(jqXHR);
                    saveBtn.prop('disabled', false);
                })
            })
        })
    </script>
</head>
<body>

<?php require_once $config['serverRoot'] . '/partials/nav.php' ?>
<main role="main" class="container_replacement" style="min-width: 1000px" >
    <div class="row">
        <div class="col-lg-9">
            <h1>Official Schedule</h1>

        </div>
    </div>

    <div class="row">
        <div class="col-3">

            <h3>Employees</h3>

            <input id="forNameSearch" type="text" autocomplete="off" class="form-control" placeholder="Type Name">
            <div class="pre-scrollable">
                <div id="employeeList"></div>
            </div>
        </div>
        <div class="col-9">
            <div class="row">
                <div class="col-2">
                    <h3>Schedule</h3>
                </div>
                <div class="dropdown col">
                    <div class="form-group row">
                        <div class="btn-group-toggle" data-toggle="buttons">
                            <label class="btn btn-outline-secondary active">
                                <input type="radio" name="options" id="lab" autocomplete="off" checked> Lab
                            </label>
                            <label class="btn btn-outline-secondary">
                                <input type="radio" name="options" id="fsr" autocomplete="off"> FSR
                            </label>
                        </div>
                    </div>
                </div>
                <div class="col">
                    <input id="startDate" autocomplete="off" placeholder="Start Date" class="form-control" type="text" value="<?= date('m/d/Y') ?>">
                </div>
                <div class="col">
                    <input id="endDate" autocomplete="off" placeholder="End Date" class="form-control" type="text">
                </div>
            </div>
            <table id="schedule" class="table table-striped table-bordered ">
                <thead>
                <tr>
                    <th>&nbsp;</th>
                    <th class="text-center" scope="col">Monday</th>
                    <th class="text-center" scope="col">Tuesday</th>
                    <th class="text-center" scope="col">Wednesday</th>
                    <th class="text-center" scope="col">Thursday</th>
                    <th class="text-center" scope="col">Friday</th>
                </tr>
                </thead>

                <?php
                for ($hour = 9; $hour < 17; $hour++) {
                    if ($hour < 12)
                        $hourStr = $hour . 'am';
                    else if ($hour === 12)
                        $hourStr = $hour . 'pm';
                    else
                        $hourStr = $hour - 12 . 'pm';

                    echo '<tr><th scope="row" class="text-nowrap">' .  $hourStr . '</th>';
                    for ($day = 1; $day < 6; $day++) {
                        $index = $day . ':' . $hour;
                        $dayStr = dayOfWeekToStr($day);
                        echo "<td data-time=\"$hour\" data-day=\"$dayStr\">";

                        if (isset($labShiftHours[$index])) {
                            foreach ($labShiftHours[$index] as $shift) {
                                $employee = $employeeMap[$shift->employeeId];
                                echo "<div data-employee-id=\"$shift->employeeId\"" .
                                    " data-first-name=\"$employee->firstName\" " .
                                    " data-last-name=\"$employee->lastName\" " .
                                    ' class="text-nowrap draggable btn btn-block btn-secondary">' .
                                    $employee->firstName . " " . $employee->lastName .
                                    '</div>';
                            }
                        }
                        echo '</td>';
                    }
                    echo '</tr>';
                }
                ?>
            </table>
            <table id="fsrSchedule" class="table table-striped table-bordered" style="display: none">
                <thead>
                <tr>
                    <th>&nbsp;</th>
                    <th class="text-center" scope="col">Monday</th>
                    <th class="text-center" scope="col">Tuesday</th>
                    <th class="text-center" scope="col">Wednesday</th>
                    <th class="text-center" scope="col">Thursday</th>
                    <th class="text-center" scope="col">Friday</th>
                </tr>
                </thead>
                <?php
                for ($hour = 9; $hour < 17; $hour++) {
                    if ($hour < 12)
                        $hourStr = $hour . 'am';
                    else if ($hour === 12)
                        $hourStr = $hour . 'pm';
                    else
                        $hourStr = $hour - 12 . 'pm';

                    echo '<tr><th scope="row" class="text-nowrap">' .  $hourStr . '</th>';
                    for ($day = 1; $day < 6; $day++) {
                        $index = $day . ':' . $hour;
                        $dayStr = dayOfWeekToStr($day);
                        echo "<td data-time=\"$hour\" data-day=\"$dayStr\">";
                        if (isset($fsrShiftHours[$index])) {
                            foreach ($fsrShiftHours[$index] as $shift) {
                                $employee = $employeeMap[$shift->employeeId];
                                echo "<div data-employee-id=\"$shift->employeeId\"" .
                                    " data-first-name=\"$employee->firstName\" " .
                                    " data-last-name=\"$employee->lastName\" " .
                                    ' class="text-nowrap draggable btn btn-block btn-secondary">' .
                                    $employee->firstName . " " . $employee->lastName .
                                    '</div>';

                            }
                        }
                        echo '</td>';
                    }
                    echo '</tr>';
                }
                ?>

            </table>
        </div>
    </div>
    <button id="saveBtn" class="btn btn-lg btn-primary offset-lg-10 col-lg-2" style="margin-bottom: 5px">Save</button>


</main>
<?php require_once $config['serverRoot'] . '/partials/footer.php' ?>
</body>
</html>
