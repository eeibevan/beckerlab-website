<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>Log In</title>

    <?php require_once $config['serverRoot'] . '/partials/scriptAndCss.php' ?>

    <style>
        #signInForm input {
            margin-top: 15px;
            margin-bottom: 10px;
        }
    </style>
    <script>
        $(document).ready(function () {
            $("#signInForm").submit(function (event) {
                event.preventDefault();
                event.stopPropagation();

                $.ajax({
                    type: "POST",
                    url: './index.php?path=/api/login',
                    dataType: 'json',
                    data: {
                        username: $("#inputUsername").val().trim(),
                        password: $("#inputPassword").val()
                    }
                }).done(function (data, status, xhr) {
                    window.location.replace($("#requestedPath").val());
                }).fail(function (jqXHR, textStatus, errorThrown) {
                    var data = jqXHR.responseJSON;
                    if (data !== undefined)
                        $("#signInError").show().html(data.message);
                })
            })
        })
    </script>
</head>
<body>

<?php require_once $config['serverRoot'] .'/partials/nav.php' ?>

<main role="main" class="container col-lg-4">
    <form id="signInForm" class="form-signin" novalidate>
        <h2 class="form-signin-heading">Please sign in</h2>
        <div id="signInError" class="alert alert-danger" role="alert" style="display: none"></div>
        <input type="text" id="inputUsername" class="form-control" placeholder="ex. beckerlab@clarion.edu" required autofocus>
        <?php
        $queryArr = [];
        // If The Path Is /login, Direct Them To The Homepage
        // Otherwise, Direct Them To Their Originally
        // Requested Page With The Necessary Parameters
        foreach ($_GET as $key => $value) {
            if ($key === 'path' && $value === '/login')
                $queryArr[$key] = '/home';
            else
                $queryArr[$key] = $value;
        }
        if (!isset($queryArr['path']))
            $queryArr['path'] = '/home';
        ?>
        <input type="hidden" id="requestedPath" value="<?=$config['webRoot']?>index.php?<?= http_build_query($queryArr) ?>">
        <input type="password" id="inputPassword" class="form-control" placeholder="Password" required>
        <button id="btnLogin" class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
    </form>
</main>

<?php require_once $config['serverRoot'] . '/partials/footer.php' ?>
</body>
</html>