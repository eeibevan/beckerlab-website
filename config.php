<?php

$config = [
    /**
     * The Base For All URL's In The Project
     * & The Sub-domain To Expose The Auth Cookies To
     *
     * In Production Should Look Like '/~beckerlab/'
     */
    'webRoot' => '/',

    /**
     * Set To Enable Options To Make The User Connect Over HTTPS
     * Should Only Be Used In Production
     */
    'forceHttps' => false,

    /**
     * Maximum Days In Advance A Non-CIS Class May Reserve The Lab
     */
    'maxNonCISReservationDays' => 14,

    /**
     * Set To True To Send Notifications For Lab Reservations & Drive Requests
     */
    'enableEmailNotifications' => true,

    /**
     * Maximum Length of Time The Cookie That Resumes A User's Session May Remain (In Seconds)
     */
    'authCookieExpires' => 30 * 24 * 3600, // One Month From Now

    /**
     * The User To Send Emails From
     */
    'emailUsername' => 'cubeckerlab@gmail.com',

    /**
     * The Password For The Account Defined By emailUsername
     */
    'emailPassword' => '',

    /**
     * The Host To Connect To Send Emails
     */
    'emailHost' => 'ssl://smtp.gmail.com',

    /**
     * Which Port To Connect To On The Host Defined By emailHost
     */
    'emailPort' => '465',

    /**
     * Array of Email Addresses To Cc Every Time An Email Is Sent By This System
     */
    'emailAdditionalCc' => ['skim@clarion.edu', 'beckerlab@clarion.edu']
];

// On CISPROD we must use CONTEXT_DOCUMENT_ROOT
// Since DOCUMENT_ROOT Is The Server wide document root
// not the user specific root
if (isset($_SERVER['CONTEXT_DOCUMENT_ROOT']))
    $config['serverRoot'] = $_SERVER['CONTEXT_DOCUMENT_ROOT'];
else
    $config['serverRoot'] = $_SERVER['DOCUMENT_ROOT'];

// If the email password is unset
// Then we cannot send emails
if (empty($config['emailPassword']))
    $config['enableEmailNotifications'] = false;

return $config;