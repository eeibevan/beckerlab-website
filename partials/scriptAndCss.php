
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
<link rel="stylesheet" type="text/css" href="<?=$config['webRoot']?>lib/jquery-ui-1.12.1/jquery-ui.css" />
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/timepicker@1.11.12/jquery.timepicker.min.css" />
<link rel="stylesheet" type="text/css" href="<?=$config['webRoot']?>node_modules/fullcalendar/dist/fullcalendar.css" />
<link rel="stylesheet" href="<?=$config['webRoot']?>css/main.css" />
<link rel="stylesheet" type="text/css" href="<?=$config['webRoot']?>node_modules/dragula/dist/dragula.css" />

<script src="<?=$config['webRoot']?>lib/jquery-3.3.1.min.js" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>
<script src="<?=$config['webRoot']?>node_modules/moment/moment.js"></script>
<script src="<?=$config['webRoot']?>lib/jquery-ui-1.12.1/jquery-ui.js"></script>
<script src="https://cdn.jsdelivr.net/npm/timepicker@1.11.12/jquery.timepicker.min.js"></script>
<script src="<?=$config['webRoot']?>node_modules/fullcalendar/dist/fullcalendar.js"></script>
<script src="<?=$config['webRoot']?>node_modules/dragula/dist/dragula.js"></script>
<script defer src="https://use.fontawesome.com/releases/v5.0.8/js/all.js" integrity="sha384-SlE991lGASHoBfWbelyBPLsUlwY1GwNDJo3jSJO04KZ33K2bwfV9YBauFfnzvynJ" crossorigin="anonymous"></script>

<?php
if ($config['forceHttps']) { ?>
<script>
    // Since We're running behind a load balance in production,
    // we cannot check for https server side
    // so, we must do it here
    if (window.location.protocol !== "https:") {
        window.location.replace("https://" + window.location.host + window.location.pathname + window.location.search);
    }
</script>
<?php } ?>

<script>
    function status401Handler() {
        window.location.href = "<?=$config['webRoot']?>index.php?<?=http_build_query($_GET)?>"
    }

    function baseAjaxErrorHandler(jqXHR, alertMessage) {
        if (jqXHR.status === 401)
            return;

        alertMessage = alertMessage || 'Error Code: ' + jqXHR.status;

        if ('responseJSON' in jqXHR && 'message' in jqXHR.responseJSON) {
            alert(jqXHR.responseJSON.message);
        } else {
            alert(alertMessage);
        }
    }
</script>
