<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <?php require_once $config['serverRoot'] .'/partials/scriptAndCss.php' ?>

    <script>
        $(document).ready(function() {
            // Init Full Calendar
            // Event Source (see: https://fullcalendar.io/docs/events-json-feed)
            $('#calendar').fullCalendar({
                themeSystem: 'bootstrap4',
                defaultView: 'agendaWeek',
                events: './index.php?path=/api/lab',
                weekends: false,
                allDaySlot: false,
                contentHeight: 'auto',
                minTime: '09:00:00',
                maxTime: '17:00:00'
            });

            var carousel = $("#carousel");
            var calendarContainer = $("#calendarContainer");
            $("#toggleCal").click(function () {
                carousel.toggle();
                calendarContainer.toggle()
            })
        })
    </script>
    <title>Becker Lab</title>
</head>
<body>

<?php require_once $config['serverRoot'] . '/partials/nav.php' ?>

<main role="main" class="container_replacement">

    <h2>Becker Lab</h2>
    <div class="row">

        <div class="col-sm-12 col-lg-4">

            <h4>Becker Hall - Room 152</h4>
            <table id="hourTable">
                <tbody>
                    <tr>
                        <td><b>Sunday</b></td>
                        <td>Closed</td>
                    </tr>
                    <tr>
                        <td><b>Monday - Thursday</td>
                        <td>9am - 5pm</td>
                    </tr>
                    <tr>
                        <td><b>Friday</b></td>
                        <td>9am - 2pm</td>
                    </tr>
                    <tr>
                        <td><b>Saturday</b></td>
                        <td>Closed</td>
                    </tr>
                </tbody>
            </table>
            <div id="carousel" class="carousel slide" data-ride="carousel">
                <ul class="carousel-indicators">
                    <li data-target="#demo" data-slide-to="0" class="active"></li>
                    <li data-target="#demo" data-slide-to="1"></li>
                    <li data-target="#demo" data-slide-to="2"></li>
                </ul>
                <div class="carousel-inner">
                    <div class="carousel-item active">
                        <img src="img/carousel/Keith.png" width="100%" alt="Keith">
                    </div>
                    <div class="carousel-item">
                        <img src="img/carousel/mainLab.png" width="100%" alt="Main Lab">
                    </div>
                    <div class="carousel-item">
                        <img src="img/carousel/teachingLab.png" width="100%" alt="Advanced Lab">
                    </div>
                </div>
                <a class="carousel-control-prev" href="#carousel" data-slide="prev">
                    <span class="carousel-control-prev-icon"></span>
                </a>
                <a class="carousel-control-next" href="#carousel" data-slide="next">
                    <span class="carousel-control-next-icon"></span>
                </a>
            </div>

        </div>

        <div id="calendarContainer" class="col-sm-12 col-lg-8">
            <div class="row d-flex justify-content-end">
                <h5><span class="badge badge-info key">Teaching</span></h5>
                <h5><span class="badge badge-success key">Outside</span></h5>
            </div>
            <div id="calendar"></div>
        </div>


    </div>
</main>

<?php require_once $config['serverRoot'] . '/partials/footer.php' ?>
</body>
</html>